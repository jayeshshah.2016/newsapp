package com.jayesh.news.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.jayesh.news.views.NewsActivity;
import com.jayesh.news.R;
import com.jayesh.news.model.Source;

import java.util.ArrayList;
import java.util.List;

public class SourceAdapter extends RecyclerView.Adapter<SourceAdapter.BaseViewHolder> {
    private static final String TAG = "Adapter";
    private Context mContext;
    private List<Source> userList;

    public SourceAdapter(List<Source> userList,Context context) {
        this.userList = userList;

        this.mContext=context;
    }
    public void setData(List<Source> userList){

        this.userList=userList;

    }
    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }
    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.source_row_item, parent, false));
    }
    @Override
    public int getItemViewType(int position) {
        return 0;
    }
    @Override
    public int getItemCount() {
        if (userList != null && userList.size() > 0) {
            return userList.size();
        } else {
            return 0;
        }
    }
    public class ViewHolder extends BaseViewHolder {

        TextView txtName;
        TextView txtDesc;
        TextView txtPhone;
        ImageView iv_article;
        public ViewHolder(View itemView) {
            super(itemView);

            txtName = itemView.findViewById(R.id.txt_name);
            txtDesc = itemView.findViewById(R.id.txt_desc);

        }
        protected void clear() {
            txtName.setText("");
            txtDesc.setText("");

        }
        public void onBind(int position) {
            super.onBind(position);
            final Source user = userList.get(position);

            if (user.getName() != null) {
                txtName.setText(user.getName());
                txtDesc.setText(user.getDescription());
            }

        }
    }

    public abstract class BaseViewHolder extends RecyclerView.ViewHolder {
        private int mCurrentPosition;
        public BaseViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(mContext, NewsActivity.class);
                    Bundle bundle=new Bundle();
                    Source clickedSource = userList.get(getAdapterPosition());


                        bundle.putParcelableArrayList("sourcedata",(ArrayList<Source>) userList);
                        bundle.putParcelable("clickedsource",clickedSource);


                    intent.putExtras(bundle);
                    mContext.startActivity(intent);
                }
            });
        }
        protected abstract void clear();
        public void onBind(int position) {
            mCurrentPosition = position;
            clear();
        }
        public int getCurrentPosition() {
            return mCurrentPosition;
        }
    }
}
