package com.jayesh.news.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.jayesh.news.R;
import com.jayesh.news.model.Article;
import com.jayesh.news.views.DetailActivity;


import java.util.List;

public class ArticleAdapter extends RecyclerView.Adapter<ArticleAdapter.BaseViewHolder> {
    private static final String TAG = "Adapter";
    private Context mContext;
    private List<Article> userList;
    public ArticleAdapter(List<Article> userList, Context context) {
        this.userList = userList;
        this.mContext=context;
    }
    public void setData(List<Article> userList){
        this.userList=userList;
    }
    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }
    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.news_row_item, parent, false));
    }
    @Override
    public int getItemViewType(int position) {
        return 0;
    }
    @Override
    public int getItemCount() {
        if (userList != null && userList.size() > 0) {
            return userList.size();
        } else {
            return 0;
        }
    }
    public class ViewHolder extends BaseViewHolder {

        TextView txtName;
        TextView txtEmail;
        TextView txtPhone;

        ImageView iv_article;
        public ViewHolder(View itemView) {
            super(itemView);

            txtName = itemView.findViewById(R.id.txt_name);
            txtEmail = itemView.findViewById(R.id.txt_email);
            txtPhone = itemView.findViewById(R.id.txt_phone);

            iv_article=itemView.findViewById(R.id.iv_article);
        }
        protected void clear() {
            txtName.setText("");
            txtEmail.setText("");
            txtPhone.setText("");
        }
        public void onBind(int position) {
            super.onBind(position);
            final Article user = userList.get(position);

            if (user.getTitle() != null) {
                txtName.setText(user.getTitle());
            }
            if (user.getContent() != null) {
                txtEmail.setText(user.getContent());
            }
            if (user.getAuthor() != null) {
                txtPhone.setText(user.getAuthor());
            }

            if(user.getUrlToImage()!=null) {
                Glide.with(mContext)
                        .load(user.getUrlToImage())
                        .into(iv_article);
            }

        }
    }

    public abstract class BaseViewHolder extends RecyclerView.ViewHolder {
        private int mCurrentPosition;
        public BaseViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(mContext, DetailActivity.class);
                    intent.putExtra("data", userList.get(getAdapterPosition()));
                    mContext.startActivity(intent);
                }
            });
        }
        protected abstract void clear();
        public void onBind(int position) {
            mCurrentPosition = position;
            clear();
        }
        public int getCurrentPosition() {
            return mCurrentPosition;
        }
    }
}
