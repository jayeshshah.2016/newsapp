package com.jayesh.news;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.jayesh.news.model.Article;

import com.jayesh.news.model.ArticleWrapper;
import com.jayesh.news.retrofit.RestApiService;
import com.jayesh.news.retrofit.RetrofitInstance;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ArticleRepository {
    private ArrayList<Article> users = new ArrayList<>();
    private MutableLiveData<List<Article>> mutableLiveData = new MutableLiveData<>();
    private Application application;
    public ArticleRepository(Application application) {
        this.application = application;
    }
    public MutableLiveData<List<Article>> getMutableLiveData(String source,MyErrorListener listener) {
        RestApiService apiService = RetrofitInstance.getApiService();
        Call<ArticleWrapper> call = apiService.getArticleList(source,Constants.KEY);
        call.enqueue(new Callback<ArticleWrapper>() {
            @Override
            public void onResponse(Call<ArticleWrapper> call, Response<ArticleWrapper> response) {

                ArticleWrapper articleWrapper = response.body();
                if (articleWrapper != null && articleWrapper.getUser() != null) {
                    users = (ArrayList<Article>) articleWrapper.getUser();
                    mutableLiveData.setValue(users);
                }
            }
            @Override
            public void onFailure(Call<ArticleWrapper> call, Throwable t) {
                listener.callback(t.getMessage());
                Log.d("ListSize"," - > Error    "+ t.getMessage());
            }
        });
        return mutableLiveData;
    }
}