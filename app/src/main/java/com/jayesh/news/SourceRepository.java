package com.jayesh.news;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;


import com.jayesh.news.model.Source;
import com.jayesh.news.model.SourceWrapper;
import com.jayesh.news.retrofit.RestApiServiceSources;
import com.jayesh.news.retrofit.RetrofitInstance;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SourceRepository {
    private ArrayList<Source> users = new ArrayList<>();
    private MutableLiveData<List<Source>> mutableLiveData = new MutableLiveData<>();
    private Application application;
    public SourceRepository(Application application) {
        this.application = application;
    }
    public MutableLiveData<List<Source>> getMutableLiveData(MyErrorListener listener) {
        RestApiServiceSources apiService = RetrofitInstance.getApiServiceSource();
        Call<SourceWrapper> call = apiService.getSourceList(Constants.KEY);
        call.enqueue(new Callback<SourceWrapper>() {
            @Override
            public void onResponse(Call<SourceWrapper> call, Response<SourceWrapper> response) {

                SourceWrapper sourceWrapper = response.body();
                if (sourceWrapper != null && sourceWrapper.getUser() != null) {
                    users = (ArrayList<Source>) sourceWrapper.getUser();
                    mutableLiveData.setValue(users);
                }
            }
            @Override
            public void onFailure(Call<SourceWrapper> call, Throwable t) {

                listener.callback(t.getMessage());
                Log.d("ListSize"," - > Error    "+ t.getMessage());
            }
        });
        return mutableLiveData;
    }
}