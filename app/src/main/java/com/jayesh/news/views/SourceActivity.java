package com.jayesh.news.views;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.res.Configuration;
import android.os.Bundle;
import android.widget.Toast;

import com.jayesh.news.MyErrorListener;
import com.jayesh.news.R;
import com.jayesh.news.model.Article;
import com.jayesh.news.model.Source;
import com.jayesh.news.viewmodel.SourceViewModel;
import com.jayesh.news.adapter.SourceAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SourceActivity extends AppCompatActivity implements MyErrorListener {

    List<Source> sourcesList = new ArrayList<>();


    RecyclerView recyclerView;
    SwipeRefreshLayout swipeRefresh;
    private SourceViewModel mainViewModel;
    SourceAdapter sourceAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_source);

        swipeRefresh = findViewById(R.id.swiperefresh);
        recyclerView = findViewById(R.id.recyclerView);

        sourceAdapter = new SourceAdapter(sourcesList,this);
        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
        } else {
            recyclerView.setLayoutManager(new GridLayoutManager(this, 4));
        }
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(sourceAdapter);
        sourceAdapter.notifyDataSetChanged();

        mainViewModel = ViewModelProviders.of(this).get(SourceViewModel.class);


        getSourceList();

        swipeRefresh.setOnRefreshListener(() -> {
            getSourceList();
        });
    }

    public void getSourceList() {
        swipeRefresh.setRefreshing(true);
        mainViewModel.getAllUsers(this).observe(this, new Observer<List<Source>>() {
            @Override
            public void onChanged(@Nullable List<Source> userList) {
                swipeRefresh.setRefreshing(false);
                setRecyclerViewData(userList);
            }
        });


    }
    private void setRecyclerViewData(List<Source> sourcesList1) {

        List<Source> sources = new ArrayList<>();
        int i =0;
        for(Source s:sourcesList1){

                sources.add(s);
                i++;
            if(i==9){
                break;
            }
        }
        sourcesList=sources;

        sourceAdapter.setData(sourcesList);
        sourceAdapter.notifyDataSetChanged();

    }

    private Map<Source,List<Article>> filterArticle(List<Article> userList,List<Source> sources){
       Map<Source,List<Article>> list = new HashMap<>();
       for(Source s: sources) {
           List<Article> articleList = new ArrayList<>();
           for (Article a : userList) {
               if (a.getSource().getId().equals(s.getId())) {
                   articleList.add(a);
               }
           }
           list.put(s,articleList);
       }

       return list;

    }

    private List<Source> setSourceData(List<Article> userList){
        List<Source> sources = new ArrayList<>();
        for(Article a:userList){
            sources.add(a.getSource());
        }
        return sources;
    }


    @Override
    public void callback(String result) {
        Toast.makeText(this, result, Toast.LENGTH_SHORT).show();
    }
}