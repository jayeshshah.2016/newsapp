package com.jayesh.news.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.jayesh.news.R;
import com.jayesh.news.model.Article;


public class DetailActivity extends AppCompatActivity {

    private Article userData;
    private TextView tv_title,tv_content,tv_url,tv_desc;
    private ImageView iv_article;
    private Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        toolbar=findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        tv_title=findViewById(R.id.tv_title);
        tv_content=findViewById(R.id.tv_content);
        tv_desc=findViewById(R.id.tv_desc);
        tv_url=findViewById(R.id.tv_url);
        iv_article=findViewById(R.id.iv_article);


        userData = (Article) getIntent().getParcelableExtra("data");
        tv_title.setText(userData.getTitle());
        tv_desc.setText(userData.getDescription());
        tv_content.setText(userData.getContent());
        tv_url.setText(userData.getUrl());
        Glide.with(this)
                .load(userData.getUrlToImage())
                .into(iv_article);

        tv_url.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!tv_url.getText().toString().trim().equals("")) {
                    Intent i = new Intent(DetailActivity.this, WebActivity.class);
                    i.putExtra("url", tv_url.getText().toString());
                    startActivity(i);
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }
}