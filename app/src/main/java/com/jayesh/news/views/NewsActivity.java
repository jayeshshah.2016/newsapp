package com.jayesh.news.views;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.res.Configuration;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.widget.LinearLayout;
import android.widget.Toast;


import com.google.android.material.tabs.TabLayout;
import com.jayesh.news.MyErrorListener;
import com.jayesh.news.R;
import com.jayesh.news.model.Article;
import com.jayesh.news.model.Source;
import com.jayesh.news.viewmodel.MainViewModel;
import com.jayesh.news.adapter.ArticleAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NewsActivity extends AppCompatActivity implements MyErrorListener {

    private TabLayout tablayout;
    private TabLayout.Tab nexttab,previoustab,currenttab;
    List<Source> sourcesList = new ArrayList<>();

    RecyclerView recyclerView;
    SwipeRefreshLayout swipeRefresh;
    private MainViewModel mainViewModel;
    ArticleAdapter articleAdapter;
    private List<Article> userList=new ArrayList<>();
    private int currentsource=0;
    private Source currentClickedSource;
    private Animation fadeIn;
    private Toolbar toolbar;
    private Map<String,List<Article>> articlesourceMap=new HashMap<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        toolbar=findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        sourcesList =getIntent().getExtras().getParcelableArrayList("sourcedata");
        userList = getIntent().getExtras().getParcelableArrayList("articledata");
        currentClickedSource = getIntent().getExtras().getParcelable("clickedsource");

        for(int i = 0;i<sourcesList.size();i++){
            if(sourcesList.get(i).getId().equals(currentClickedSource.getId())){
                currentsource=i;
                break;
            }
        }
        tablayout=findViewById(R.id.tablayout);
        tablayout.setSelectedTabIndicatorColor(this.getResources().getColor(R.color.secondary));

        nexttab=tablayout.newTab();
        nexttab.setText("Next Source");
        previoustab=tablayout.newTab();
        previoustab.setText("Previous Source");
        currenttab = tablayout.newTab();

        tablayout.addTab(previoustab);
        tablayout.addTab(currenttab);
        tablayout.addTab(nexttab);

        swipeRefresh = findViewById(R.id.swiperefresh);
        recyclerView = findViewById(R.id.recyclerView);

        articleAdapter = new ArticleAdapter(userList,this);
        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
        } else {
            recyclerView.setLayoutManager(new GridLayoutManager(this, 4));
        }
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(articleAdapter);
        articleAdapter.notifyDataSetChanged();

        mainViewModel = ViewModelProviders.of(this).get(MainViewModel.class);




        swipeRefresh.setOnRefreshListener(() -> {
            getArticleList();
        });

        tablayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if(tab.getPosition()==0){
                    setSourceText(currentsource,-1,sourcesList);

                }
                if(tab.getPosition()==2){
                    setSourceText(currentsource,1,sourcesList);

                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        setSourceText(currentsource,0,sourcesList);

    }

    public void getArticleList() {
        swipeRefresh.setRefreshing(true);
        mainViewModel.getAllUsers(getSourceListString(sourcesList),this).observe(this, new Observer<List<Article>>() {
            @Override
            public void onChanged(@Nullable List<Article> userList) {


                    setRecyclerViewData(userList);

            }
        });


    }

    private String getSourceListString(List<Source> sources) {
        String str_sources = "";

        str_sources=sources.get(currentsource).getId();
        return str_sources;
    }

    private void setRecyclerViewData(List<Article> userList1) {
        userList=userList1;

        articleAdapter.setData(userList1);
        articleAdapter.notifyDataSetChanged();
        swipeRefresh.setRefreshing(false);

    }

    private void filterArticle(List<Article> userList,Source sources){
        articlesourceMap.clear();
        for(Source s:sourcesList) {
            List<Article> art = new ArrayList<>();
            for (Article article : userList) {
                if(article.getSource()!=null){
                    if(article.getSource().getId().equals(s.getId())){
                        art.add(article);
                    }
                }
            }
            articlesourceMap.put(s.getId(),art);
        }
    }



    private void setSourceText(int currPos,int nextposi,List<Source> sources){
        if(currPos+nextposi==0){
            ((LinearLayout)previoustab.view).setVisibility(View.GONE);
            ((LinearLayout)nexttab.view).setVisibility(View.VISIBLE);
            currenttab.setText(sources.get(currPos+nextposi).getName());
            currenttab.select();

        }else if(currPos+nextposi==sources.size()-1){
            currenttab.select();
            ((LinearLayout)previoustab.view).setVisibility(View.VISIBLE);
            ((LinearLayout)nexttab.view).setVisibility(View.GONE);
            currenttab.setText(sources.get(currPos+nextposi).getName());

        }else{
            currenttab.select();
            ((LinearLayout)previoustab.view).setVisibility(View.VISIBLE);
            ((LinearLayout)nexttab.view).setVisibility(View.VISIBLE);
            currenttab.setText(sources.get(currPos+nextposi).getName());

        }
        currentsource=currPos+nextposi;
        getArticleList();

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }


    @Override
    public void callback(String result) {
        Toast.makeText(this, result, Toast.LENGTH_SHORT).show();
    }
}