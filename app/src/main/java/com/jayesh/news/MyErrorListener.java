package com.jayesh.news;

import android.view.View;

public interface MyErrorListener {
    // you can define any parameter as per your requirement
    public void callback(String result);
}