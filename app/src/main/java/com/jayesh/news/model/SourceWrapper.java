package com.jayesh.news.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@SuppressWarnings("unused")
public class SourceWrapper {
    @SerializedName("sources")
    private List<Source> mData;



    @SerializedName("status")
    private String mStatus;

    public List<Source> getUser() {
        return mData;
    }

    public void setUser(List<Source> data) {
        mData = data;
    }




    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }
}
