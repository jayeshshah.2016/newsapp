package com.jayesh.news.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@SuppressWarnings("unused")
public class ArticleWrapper {
    @SerializedName("articles")
    private List<Article> mData;


    @SerializedName("totalResults")
    private int totalResults;

    @SerializedName("status")
    private String mStatus;

    public List<Article> getUser() {
        return mData;
    }

    public void setUser(List<Article> data) {
        mData = data;
    }


    public int getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(int totalResults) {
        this.totalResults = totalResults;
    }


    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }
}
