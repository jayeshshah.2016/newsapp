package com.jayesh.news.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.jayesh.news.ArticleRepository;
import com.jayesh.news.MyErrorListener;
import com.jayesh.news.SourceRepository;
import com.jayesh.news.model.Article;
import com.jayesh.news.model.Source;

import java.util.List;

public class SourceViewModel extends AndroidViewModel {
    private SourceRepository sourceRepository;
    public SourceViewModel(@NonNull Application application) {
        super(application);
        sourceRepository = new SourceRepository(application);
    }
    public LiveData<List<Source>> getAllUsers(MyErrorListener listener) {
        return sourceRepository.getMutableLiveData(listener);
    }
}