package com.jayesh.news.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.jayesh.news.ArticleRepository;
import com.jayesh.news.MyErrorListener;
import com.jayesh.news.model.Article;


import java.util.List;

public class MainViewModel extends AndroidViewModel {
    private ArticleRepository articleRepository;
    public MainViewModel(@NonNull Application application) {
        super(application);
        articleRepository = new ArticleRepository(application);
    }
    public LiveData<List<Article>> getAllUsers(String source, MyErrorListener listener) {
        return articleRepository.getMutableLiveData(source,listener);
    }
}