package com.jayesh.news.retrofit;

import com.jayesh.news.model.ArticleWrapper;
import com.jayesh.news.model.Source;
import com.jayesh.news.model.SourceWrapper;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RestApiServiceSources {
    @GET("v2/sources")
    Call<SourceWrapper> getSourceList(
            @Query("apikey")
                    String apiKey
    );
}
