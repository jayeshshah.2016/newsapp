package com.jayesh.news.retrofit;



import com.jayesh.news.model.ArticleWrapper;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RestApiService {
    @GET("v2/everything/")
    Call<ArticleWrapper> getArticleList(
            @Query("sources")
                    String query,
            @Query("apikey")
                    String apiKey
    );
}

